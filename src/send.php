<?php

namespace SymfonyUtil\Component\HttpFoundation;

use Symfony\Component\HttpFoundation\Response;

function send(Response $r): Response
{
    return (clone $r)->send();
}

